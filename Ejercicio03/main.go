// condicionales
package main

import "fmt"

var estado bool
var numero int

func main() {
	estado = true
	//asignar un valor en la misma estruccion
	if estado = false; estado {
		fmt.Println(estado)
	} else {
		fmt.Println("Estado es falso")
	}
	/*switch*/
	switch numero = 10; numero {
	case 1:
		fmt.Println(1)
	case 2:
		fmt.Println(2)
	case 3:
		fmt.Println(3)
	case 4:
		fmt.Println(4)
	case 5:
		fmt.Println(5)
	case 6:
		fmt.Println(6)
	case 7:
		fmt.Println(7)
	default:
		fmt.Println("Mayor a 5")
	}
}
