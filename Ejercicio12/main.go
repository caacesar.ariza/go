package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	leoArchivo()
	leoArchivo2()
	graboArchivo()
	graboArchivo2()
}

func leoArchivo() {
	archivo, err := ioutil.ReadFile("./Archivos.txt")

	if err != nil {
		fmt.Printf("Error accediendo al archivo %s \n", err)
	} else {
		fmt.Println(string(archivo))

	}
}
func leoArchivo2() {
	archivo, err := os.Open("./Archivos.txt")

	if err != nil {
		fmt.Printf("Error accediendo al archivo %s \n", err)
	} else {
		scanner := bufio.NewScanner(archivo)
		for scanner.Scan() {
			registro := scanner.Text()
			fmt.Printf("Linea > %s \n", registro)
		}
	}
	archivo.Close()
}

func graboArchivo() {
	archivo, err := os.Create("./pruebaArchivo.txt")
	if err != nil {
		fmt.Printf("Error accediendo al archivo %s \n", err)
		return
	}
	fmt.Fprintln(archivo, "esta es la linea nueva")
	archivo.Close()
}
func graboArchivo2() {
	fieleName := "./pruebaArchivo.txt"
	if Append(fieleName, "\n esta es una segunda linea ") == false {
		fmt.Println("error en la segunda linea")
	}
}

func Append(nombreArchivo string, texto string) bool {
	arch, err := os.OpenFile(nombreArchivo, os.O_WRONLY|os.O_APPEND, 7777)
	if err != nil {
		fmt.Printf("Error accediendo al archivo %s \n", err)
		return false
	}
	_, err = arch.WriteString(texto)
	if err != nil {
		fmt.Printf("Error accediendo al archivo %s \n", err)
		return false
	}
	return true
}
