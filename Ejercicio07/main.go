package main

import "fmt"

/*
funciones en go cuando se desarrolla las funcionnes de parametros y parametros de entrada variables 
*/

func main() {
	fmt.Println(uno(5))
	numero, stado := dos(2)
	fmt.Println(stado)
	fmt.Println(numero)
	fmt.Println(unoExtendido(5))
	fmt.Println(calculos(5, 46))
	fmt.Println(calculos(2, 23, 45, 68))
	fmt.Println(calculos(5))
	fmt.Println(calculo(5, 46, 17, 25, 26, 98, 17))
}
func uno(numero int) int {
	return numero * 2
}
func dos(numero int) (int, bool) {
	if numero == 1 {
		return 5, false
	} else {
		return 10, true
	}
}

func unoExtendido(numero int) (z int) {
	z = numero * 2
	return
}

//funciones de parametros variables

func calculos(numeros ...int) int {
	total := 0
	for _, num := range numeros {
		total = total + num
	}
	return total
}

func calculo(numeros ...int) int {
	total := 0
	for item, num := range numeros {
		total = total + num
		fmt.Printf("\n  item %d \n", item)
	}
	return total
}
