package main

/*Recursion */
import "fmt"

func main() {
	exponente(2)
}
func exponente(numero int) {
	if numero > 1000000000 {
		return
	}
	fmt.Printf("Numero %d \n", numero)
	exponente(numero * 2)
}
