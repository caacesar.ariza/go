/*
Arreglos staticos y slice
*/
package main

import "fmt"

var table [10]int
var matriz [5][7]int

func main() {

	table[0] = 5

	table2 := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(table)
	fmt.Println(table2)
	for i := 0; i < len(table2); i++ {
		fmt.Println(table2[i])
	}
	///matriz
	matriz[1][4] = 1

	fmt.Println(matriz)
	//matriz dinamica

	matrizDinamica := []int{1, 2, 3}
	fmt.Println(matrizDinamica)
	variante2()
	variante3()
	variante4()
}
func variante2() {
	elementos := [5]int{1, 2, 3, 4, 5}
	porcion := elementos[3:]
	porcion2 := elementos[2:4]
	porcion3 := elementos[:4]
	fmt.Println(porcion)
	fmt.Println(porcion2)
	fmt.Println(porcion3)
}
func variante3() {
	elementos := make([]int, 10, 20)
	fmt.Printf("largo  %d , capacidad  %d, \n", len(elementos), cap(elementos))
}
func variante4() {
	nums := make([]int, 0, 0)
	fmt.Println(nums)
	for i := 0; i < 100; i++ {
		nums = append(nums, i)
	}

	fmt.Printf("\n Largo %d, capacidad %d \n", len(nums), cap(nums))
}
