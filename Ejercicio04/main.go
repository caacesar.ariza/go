package main

import (
	"bufio"
	"fmt"
	"os"
)

var numero1 int
var numero2 int
var resultado int
var leyenda string

func main() {
	fmt.Println("Ingrese numero 1:")
	// tomar el valor por consola -> sin embargo esta funcion  se realiza para linux
	//fmt.Scanf("%d", &numero1)
	fmt.Scanln(&numero1)
	fmt.Println("Ingrese numero 2:")
	//fmt.Scanf("%d", &numero2)
	fmt.Scanln(&numero2)

	fmt.Printf("%d \n", numero1+numero2)

	/*Alternativa con paquetes de bufio y os */
	fmt.Println("Ingrese una descripcion")
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		leyenda = scanner.Text()
	}
	resultado = numero1 + numero2
	fmt.Println(leyenda, resultado)

}
