package main

/*
estructuras en go
*/
import (
	"fmt"
	"time"

	us "user/user"
)

type empleado struct {
	us.Usuario
}

func main() {
	u := new(empleado)
	u.AltaUsuario(1, "pepe", time.Now(), true)
	fmt.Println(u.Usuario)
}
