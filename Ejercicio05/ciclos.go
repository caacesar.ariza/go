package main

import "fmt"

/*
*Desarrollo de ciclos
 */
func main() {

	i := 1
	for i < 10 {
		fmt.Print(i)
		i++
	}
	/*for tradicional
	 */
	count := 5
	for i := 0; i < count; i++ {
		fmt.Println(i)
	}

	/*form infinito
	 */
	numero := 0
	for {
		fmt.Println("continue")
		fmt.Println("Ingrese el numero secreto")
		fmt.Scanln(&numero)
		if numero == 100 {
			break
		}
	}

	//
	var j = 0
	for j < 10 {
		fmt.Printf("\n valor de i : %d ", j)
		if j == 5 {
			fmt.Printf("multiplicamos x 2 \n")
			j = j * 2
			continue
		}
		fmt.Printf(" 	paso por aqui \n")
		j++
	}

	fmt.Println()
	///
	var t int = 0

RUTINA:
	for t < 10 {
		if t == 4 {
			t = t + 2
			fmt.Printf("voy a la rutina sumando 2 a i \n ")
			goto RUTINA
		}
		fmt.Printf("valor  de t : %d \n", t)
		t++
	}
	fmt.Println(t)
}
