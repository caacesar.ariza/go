package main

import "fmt"

/*
son interceptores  que permiten ejecutar instrucciones  comunes  a varias funciones  que perciben y debuelven  los mismos  tipos de variables
*/
var result int

func main() {

	result = operacionsMidelware(sumar)(2, 3)
	fmt.Println(result)

	result = operacionsMidelware(restar)(2, 3)
	fmt.Println(result)

	result = operacionsMidelware(dividir)(2, 3)
	fmt.Println(result)

	result = operacionsMidelware(multiplicar)(2, 3)
	fmt.Println(result)

}

func sumar(a, b int) int {
	return a + b
}
func restar(a, b int) int {
	return a - b
}
func multiplicar(a, b int) int {
	return a * b
}
func dividir(a, b int) int {
	return a / b
}
func operacionsMidelware(f func(int, int) int) func(int, int) int {
	return func(a, b int) int {
		fmt.Println("Inicio operacion ")
		return f(a, b)
	}
}
