package main

/*Manejo de errores en go*/
import (
	"fmt"
	"log"
	"os"
)

func main() {

	ejemploPanico()
	archivo := "prueba.txt"
	f, err := os.Open(archivo)

	defer f.Close()
	if err != nil {
		fmt.Println("error accediendo al archivo")
		os.Exit(1)
	}

}

func ejemploPanico() {
	defer func() {
		reco := recover()
		if reco != nil {
			log.Fatalf("ocurrio un error que genero panic\n %v", reco)
		}
	}()

	a := 1
	if a == 1 {
		panic("se encontro el valor de 1")
	}
}
