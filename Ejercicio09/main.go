/*mapas */
package main

import "fmt"

func main() {
	paises := make(map[string]string)
	fmt.Println(paises)
	paises["MExico"] = "D.F"
	paises["Argentina"] = "Buenos Aires"
	fmt.Println(paises["MExico"])
	fmt.Println(paises)

	campeonato := map[string]int{
		"Argentina":    56,
		"Barcelona":    36,
		"Real Madrid":  98,
		"El chivas":    89,
		"Boca juniors": 30}

	fmt.Println(campeonato)

	campeonato["River plata"] = 25
	campeonato["El chivas"] = 90
	delete(campeonato, "Real Madrid")
	fmt.Println(campeonato)
	fmt.Print("\n \t Campeonato \n")
	for equipo, puntaje := range campeonato {
		fmt.Printf("\t El equipo %s tiene un puntaje %d \n", equipo, puntaje)
	}

	puntaje, existe := campeonato["El chivas"]
	fmt.Printf("El puntaje capturado %d , y el equipo existe %t  \n", puntaje, existe)
}
