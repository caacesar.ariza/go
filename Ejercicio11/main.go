package main

import "fmt"

type servivo interface {
	esServivo() bool
}
type humano interface {
	respirar()
	pensar()
	comer()
	sexo() string
	esServivo() bool
}
type animal interface {
	respirar()
	comer()
	EsCarnivoro() bool
	esServivo() bool
}
type vegetal interface {
	clasificacionVegetal() string
}

/*Genero humano*/

type Hombre struct {
	edad       int
	altura     float32
	peso       float32
	respirando bool
	pensando   bool
	comiendo   bool
	esHombre   bool
	vivo       bool
}
type Mujer struct {
	Hombre
	/*
		Mejora de codigo
		-----------------
		edad       int
		altura     float32
		peso       float32
		respirando bool
		pensando   bool
		comiendo   bool*/
}

func (h *Hombre) respirar()       { h.respirando = true }
func (h *Hombre) comer()          { h.comiendo = true }
func (h *Hombre) esServivo() bool { return h.vivo }
func (h *Hombre) pensar()         { h.pensando = true }
func (h *Hombre) sexo() string {
	if h.esHombre {
		return "Hombre"
	} else {
		return "Mujer"
	}
}

/*
mejorar el codigo
func (m *Mujer) respirar()     { m.respirando = true }
func (m *Mujer) comer()        { m.comiendo = true }
func (m *Mujer) pensar()       { m.pensando = true }
func (m *Mujer) sexo() string  { return "Mujer" }
*/

func HumanosRespirando(hu humano) {
	hu.respirar()
	fmt.Printf("soy un/a %s, y ya estoy respirando \n", hu.sexo())
}

/*Genero animal*/
type perro struct {
	respirando bool
	comiendo   bool
	carnivoro  bool
	vivo       bool
}

func (p *perro) respirar()         { p.respirando = true }
func (p *perro) comer()            { p.comiendo = true }
func (p *perro) EsCarnivoro() bool { return p.carnivoro }
func (p *perro) esServivo() bool   { return p.vivo }

func AnimalesRespirar(an animal) {
	an.respirar()
	fmt.Println("soy un animal y estoy respirando ")

}

func AnumalEsCarnivoro(an animal) int {
	if an.EsCarnivoro() {
		return 1
	}
	return 0
}

/*Ser vivo */
func estoyVivo(v servivo) bool {
	return v.esServivo()
}

func main() {
	Pedro := new(Hombre)
	/*Mejora de codigo */
	Pedro.esHombre = true
	/*fin de la mejora*/
	HumanosRespirando(Pedro)
	fmt.Printf("Estoy vivo %t \n", estoyVivo(Pedro))

	Maria := new(Mujer)
	HumanosRespirando(Maria)
	fmt.Printf("Estoy vivo %t \n", estoyVivo(Maria))
	/*Animal */

	totalCarnivoros := 0
	dogo := new(perro)
	dogo.vivo = true
	dogo.carnivoro = true
	AnimalesRespirar(dogo)
	totalCarnivoros = +AnumalEsCarnivoro(dogo)
	fmt.Printf("total carnivoros %d \n", totalCarnivoros)
	fmt.Printf("Estoy vivo %t \n", estoyVivo(dogo))

}
