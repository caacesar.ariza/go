package main

import "fmt"

/*
funciones anonimas
*/

var Calculo func(int, int) int = func(i1, i2 int) int {
	return i1 + i2
}

func main() {
	fmt.Printf("sumo 5 +7 = %d \n", Calculo(5, 7))
	//restamos
	Calculo = func(i1, i2 int) int {
		return i1 - i2
	}
	fmt.Printf("restamos 5-7 = %d \n", Calculo(5, 7))
	//dividimos
	Calculo = func(i1, i2 int) int {
		return i1 / i2
	}
	fmt.Printf("Dividimos 5 / 7 = %d \n", Calculo(5, 7))
	//muliplicamos
	Calculo = func(i1, i2 int) int {
		return i1 * i2
	}
	fmt.Printf("muliplicamos 5 * 7 = %d \n", Calculo(5, 7))
	Operaciones()

	/*closiurs*/
	tablaDel := 2
	Mitabla := Tabla(tablaDel)
	for i := 1; i < 11; i++ {
		fmt.Println(Mitabla())
	}

}
func Operaciones() {
	resultado := func() int {
		var a int = 32
		var b int = 89
		return a + b
	}
	fmt.Println(resultado())
}

///closuir

func Tabla(valor int) func() int {
	numero := valor
	secuencia := 0
	return func() int {
		secuencia++
		return numero * secuencia
	}
}
