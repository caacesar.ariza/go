package main

import (
	"fmt" 
	"strconv"
)

//importaciones de liberiras
//fmt para fomatos de salida

// variables
var numero int
var texto string
var status bool

func main() {

	//variables
	var numero2 int
	numero3 := 4

	numero4, numero5, numero6, numero7, texto2, status2 := 4, 5, 5, 5, "texto", true

	var flotante float64 = 0.0
	var moneda int64 = 0
	/// convertir numericos

	numero2 = int(flotante)

	//convertir a texto

	texto = fmt.Sprintf("%d", moneda)

	texto2 =strconv.Itoa(int(moneda))

	fmt.Println(numero)
	fmt.Println(numero2)
	fmt.Println(numero3)
	fmt.Println(numero4)
	fmt.Println(numero5)
	fmt.Println(numero6)
	fmt.Println(numero7)
	fmt.Println(texto)
	fmt.Println(texto2)
	fmt.Println(status)
	fmt.Println(status2)
	mostrarStatus()
}

///mostrar funciones

func mostrarStatus() {
	fmt.Println(status)
}
